# WILLKOMMEN! #

Wenn Du dies hier liest, dann bist du auf einer falschen Seite gelandet,
weil ich, der Herausgeber, in der ersten Auflage des Buches einen Fehler
gemacht habe.

### Die richtige Webseite ist hier: 

[Iris Johansson, Gemeinsam im Gespräch ... über Irrgefühl](https://bitbucket.org/epicurus/iris-irrgefuehl)

